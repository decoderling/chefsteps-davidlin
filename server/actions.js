/**
 * Singular place for API Actions
 *
 * By: David Lin
 */

// include libs

const possibleCharacters = 'abcdefghijklmnopqrstuvwxyz';
let emailLength = 8;

// generates a singular random email
const generateEmail = () => {
    var mailbox = '';
    var domain = '';

    for(var i = 0; i < emailLength; i++) {
        mailbox += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
        domain += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
    }

    return mailbox + '@' + domain + '.com';
}

// this generates a count of emails with a random percent duplicated
const generateEmails = (count) => {
    const randomCount = Math.floor(count / 2); // this will take about 50% as random

    let emails = [];
    let random = 0; // counter for random emails added to the list

    if (typeof count === 'number') {
        while(emails.length < count) {
            let email = generateEmail();

            emails.push(email);

            // if we have more room and haven't hit the random count
            if (emails.length < count && random < randomCount) {
                let index = Math.floor(Math.random() * randomCount);

                emails.splice(index, 0, email);
            }
        }
    }

    return emails;
}

// dedupes an array of strings preserving order
const dedupe = (list) => {
    let seen = {};
    let clean = [];

    for(var i = 0; i < list.length; i++) {
        let item = list[i];

        if (!seen[item]) {
            clean.push(item);
            seen[item] = true;
        }
    }

    return clean;
}

export { generateEmails, dedupe };

