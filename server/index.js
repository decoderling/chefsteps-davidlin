/**
 * Simple API Server to generate emails and dedupe them
 *
 * By: David Lin
 */

// Include libs
import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';

// Include web and api routes
import web from './web.js';
import api from './api.js';

// app and api server
const app = express();

// ensure we parse the post body as json
app.use(bodyParser.json());
app.use(express.static('./build'));

// main routes to serve html
app.use('/', web);

// api routes
app.use('/api', api);

// start api server
const server = app.listen(process.env.PORT || 3000, '0.0.0.0', () => {
    const { address, port } = server.address();
    console.log(`API Server listening at http://${address}:${port}`);
});

export default app;

