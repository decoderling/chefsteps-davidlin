/**
 * Web Routes Actions
 *
 * By: David Lin
 */

// include libs
import express from 'express';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';

import routes from '../client/routes.js';
import html from './html/index.html';
import path from 'path';

// web items
const router = express.Router();

// ------ [ Web routes ] ------
// Web index
router.get('/', (req, res) => {
    match({ routes, location: req.originalUrl }, (error, redirect, renderProps) => {
        if (error) {
            res.status(500).send('Something bad happened :(');
        } else if (redirect) {
            res.redirect(302, redirect.pathname + redirect.search());
        } else if (renderProps) {
            res.status(200).send(html);
        }
    });
});

export default router;

