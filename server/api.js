/**
 * API Routes Actions
 *
 * By: David Lin
 */

// include libs
import express from 'express';

import { generateEmails, dedupe } from './actions.js';

// api items
const router = express.Router();

// ------ [ API routes ] ------
// generate list of emails
router.get('/generate/:count', (req, res) => {
    const count = parseInt(req.params.count, 10);

    if (typeof count === 'number') {
        let emails = generateEmails(count);

        res.status(200).send(emails);
    } else {
        res.status(400).send({ 
            error: {
                code: 400,
                message: 'Invalid :count parameter (' + count + ')passed to /api/generate, should be an integer'
            } 
        });
    }
});

// dedupe emails
router.post('/dedupe', (req, res) => {
    const emails = req.body;
    const deduped = dedupe(emails);

    res.status(200).send(deduped);
});

export default router;

