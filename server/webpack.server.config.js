/**
 * Webpack Server configuration
 * Builds API and Server
 *
 * By: David Lin
 */

// include libs
var path = require('path');
var fs = require('fs');
var webpack = require('webpack');

// Dont't include the unnecessary depedencies from node modules binaries
var nodeModules = {};

fs.readdirSync('node_modules')
    .filter(function(x) { return ['.bin'].indexOf(x) === -1; })
    .forEach(function(module) {
        nodeModules[module] = 'commonjs ' + module;
    });

// app specific information
var app = {
    build: {
        path: path.resolve(__dirname, '../build')
    },
    server: {
        path: path.resolve(__dirname, '.')
    }
};

// the main configuration
var config = {
    target: 'node',
    entry: {
        server: app.server.path
    },
    output: {
        path: app.build.path,
        filename: '[name].bundle.js'
    },
    externals: nodeModules,
    module: {
        loaders: [
            { // transpile ES6
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: [ 'es2015', 'react' ]
                }
            },
            { // include for es6 express builds
                test: /\.json$/,
                loader: 'json-loader'
            },
            { // for including html files
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
                warnings: false
            }
        })
    ]
};

module.exports = config;

