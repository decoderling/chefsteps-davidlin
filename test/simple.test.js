/**
 * Simple Tests to verify the following:
 *
 * Leave the resulting list in the original order (same as the input).
 * The function should be able to handle 100,000 email addresses containing 50% randomly placed duplicates in well under 1 second on a typical modern laptop.
 *
 * By: David Lin
 */

// include libs
let path = require('path');
let chai = require('chai');
var expect = chai.expect;

// items to test
let { generateEmails, dedupe } = require(path.join(__dirname, '..', 'server', 'actions.js'));

const emails = generateEmails(100000); // generate 100,000 emails

describe('Simple Tests', () => {
    it('should leave the resulting deduped list in the original order', () => {
        const deduped = dedupe([ // dedupe the list of emails
            'foo@bar.com',
            'baz@bax.com',
            'foo@bar.com',
            'baz@bax.com',
            'my@example.com',
            'foo@bar.com',
            'baz@bax.com',
            'my@example.com'
        ]);

        expect(deduped).to.have.length(3);
        expect(deduped).to.eql([
            'foo@bar.com',
            'baz@bax.com',
            'my@example.com'
        ]);
    });

    it('should dedupe 100,000 emails in under 1000ms (1 second)', function(done) {
        this.timeout(1000); // set the timeout duration
        const deduped = dedupe(emails);
        expect(deduped).to.be.an('array');
        done();
    });
});

