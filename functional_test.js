/**
 * Dummy script to test the performance of my dedupe function
 *
 * By: David Lin
 */

// include libs
import { generateEmails, dedupe } from './server/actions.js';

const count = 100000;

console.log('Generating %d emails...', count);

const emails = generateEmails(count); // generate the list of emails

console.log('Deduplicating the list of emails...\n');
console.time('dedupe');
const deduped = dedupe(emails); // run function
console.timeEnd('dedupe');

console.log('\nDone!\n');

