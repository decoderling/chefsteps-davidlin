/**
 * Webpack configuration
 * Builds API and UI bundle
 *
 * By: David Lin
 */

// include configs
var client = require('./client/webpack.client.config.js');
var server = require('./server/webpack.server.config.js');

module.exports = [client, server];

