/**
 * The Dedupe component
 *
 * By: David Lin
 */

// include libs
import React, { Component } from 'react';

// include child components
import Emails from './emails.jsx';
import Results from './results.jsx';

class Dedupe extends Component {
    constructor(props) {
        super(props);

        // save instance scope for handler functions
        this.handleGenerate = this.handleGenerate.bind(this);
        this.handleDedupe = this.handleDedupe.bind(this);
        this.getEmails = this.getEmails.bind(this);
        this.getDeduped = this.getDeduped.bind(this);
    }

    // retrieve the list of emails
    getEmails() {
        return this.props.emails;
    }

    // retrieve the list of deduped emails
    getDeduped() {
        return this.props.deduped;
    }

    // event handler for generate onClick
    handleGenerate(count) {
        this.props.generateEmails(count);
    }

    // event handler for dedupe onClick
    handleDedupe() {
        this.props.dedupeEmails(this.getEmails());
    }

    render() {
        return (
            <div className="dedupe">
                <div className="intro">Email Deduplication</div>
                <Emails onClick={this.handleGenerate} getEmails={this.getEmails} />
                <Results dedupeEmails={this.handleDedupe} getEmails={this.getEmails} getDeduped={this.getDeduped} />
            </div>
        );
    }
}

// Default props
Dedupe.defaultProps = {
    count: 4,
    emails: [],
    deduped: []
};

export default Dedupe;

