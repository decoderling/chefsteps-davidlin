/**
 * The Main Application component
 *
 * By: David Lin
 */

// include libs
import React, { Component } from 'react';

import Header from './header.jsx';
import DedupeList from '../containers/dedupe_list.js';

class Main extends Component {
    render() {
        return (
            <div className="main">
                <Header />
                <DedupeList />
            </div>
        );
    }
}

export default Main;

