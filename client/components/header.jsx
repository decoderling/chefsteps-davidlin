/**
 * The Header component
 *
 * By: David Lin
 */

// include libs
import React, { Component } from 'react';

import FaSearch from 'react-icons/lib/fa/search';

class Header extends Component {
    render() {
        return (
            <nav className="navigation">
                <div className="logo">
                    <a href="/">
                        <img src="/chefsteps-logo-h.png" />
                    </a>
                </div>
                <div className="title">
                    <span>Email Deduplication</span>
                </div>
                <div className="right">
                    <a className="search"><FaSearch /></a>
                    <a className="sign-in" href="#">Sign In</a>
                </div>
            </nav>
        );
    }
}

export default Header;

