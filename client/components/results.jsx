/**
 * The Results component
 *
 * By: David Lin
 */

// include libs
import React, { Component, PropTypes } from 'react';

class Results extends Component {
    constructor(props) {
        super(props);

        // save instance scope for handler functions
        this.handleDedupe = this.handleDedupe.bind(this);
    }

    // event handler for deduping emails
    handleDedupe() {
        this.props.dedupeEmails(this.props.getEmails());
    }

    // generate JSX for the emails to display
    renderDeduped() {
        const deduped = this.props.getDeduped();

        if (deduped.length === 0) {
            return (
                <li className="empty">Click to generate emails to dedupe.</li>
            );
        } else {
            return (
                deduped.map(email => 
                    <li>{ email }</li>
                )
            );
        }
    }

    render() {
        const deduped = this.renderDeduped();

        return (
            <div className="results">
                <span className="dedupe-button" onClick={this.handleDedupe}>Dedupe!</span>
                <div className="email-results">
                    <ul>{ deduped }</ul>
                </div>
            </div>
        );
    }
}

Results.propTypes = {
    dedupeEmails: PropTypes.func.isRequired,
    getEmails: PropTypes.func.isRequired
};

export default Results;

