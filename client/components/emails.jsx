/**
 * The Emails component
 *
 * By: David Lin
 */

// include libs
import React, { Component, PropTypes } from 'react';

class Emails extends Component {
    constructor(props) {
        super(props);

        // save instance scope for handler functions
        this.handleOnClick = this.handleOnClick.bind(this);
        this.toggleEmailVisibility = this.toggleEmailVisibility.bind(this);

        // initial state values
        this.state = {
            showEmails: false // initial value
        };
    }

    // retrieve the count from the input element
    getCount() {
        return this.refs.count.value;
    }

    // event handler for button onClick
    handleOnClick() {
        this.props.onClick(this.getCount());
        this.toggleEmailVisibility();
    }

    // toggle visibility of the generated emails
    toggleEmailVisibility() {
        const showEmails = !this.state.showEmails; // reverse the current state

        this.setState({ showEmails }); // set the reversed state
    }

    // generate JSX for the emails to display
    renderEmails() {
        const emails = this.props.getEmails();

        if (emails.length === 0) {
            return (
                <li className="empty">No emails yet, click to generate some!</li>
            );
        } else {
            return (
                emails.map(email => 
                    <li>{ email }</li>
                )
            );
        }
    }

    render() {
        const emails = this.renderEmails();

        return (
            <div className="generate-emails">
                <label htmlFor="count">Number of emails to generate
                    <input type="number" id="count" ref="count" />
                </label>
                <span className="generate-button" onClick={this.handleOnClick}>Generate Emails</span>
                <div className="emails">
                    <div className="emails-control">
                        <span className="toggle-emails" onClick={this.toggleEmailVisibility}>{ this.state.showEmails ? 'Hide' : 'Show' } Emails</span>
                    </div>
                    { this.state.showEmails ? <ul>{ emails }</ul> : null }
                </div>
            </div>
        );
    }
}

Emails.propTypes = {
    getEmails: PropTypes.func.isRequired
};

export default Emails;

