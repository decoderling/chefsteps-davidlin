/**
 * Main React Routes for the client
 *
 * By: David Lin
 */

// include libs
import React from 'react';
import { Route, IndexRoute, Router, browserHistory } from 'react-router';

// include React components/containers
import Main from './components/main.jsx';

export default (
    <Router history={ browserHistory }>
        <Route path="/" component={Main} />
    </Router>
);

