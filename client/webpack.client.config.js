/**
 * Webpack UI configuration
 * Builds UI bundle
 *
 * By: David Lin
 */

// include libs
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

// app specific information
var app = {
    build: {
        path: path.resolve(__dirname, '../build')
    },
    client: {
        path: path.resolve(__dirname, '.')
    },
    icons: {
        path: path.resolve(__dirname, './node_modules/react-icons/fa')
    }
};

// the main configuration
var config = {
    entry: {
        client: app.client.path + '/index.js'
    },
    output: {
        path: app.build.path,
        filename: '[name].bundle.js'
    },
    module: {
        loaders: [
            { // transpile ES6
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: [ 'es2015', 'react' ]
                }
            },
            { // transpile ES6 react-icons
                test: /\.jsx?$/,
                loader: 'babel',
                include: [
                    app.icons.path
                ],
                query: {
                    presets: [ 'es2015', 'react' ]
                }
            },
            { // compile sass to css into a separate file
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css!sass')
            },
            { // for including html files
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
                warnings: false
            }
        }),
        new ExtractTextPlugin('bundle.css')
    ]
};

module.exports = config;

