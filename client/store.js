/**
 * Redux Store to contain the emails
 *
 * By: David Lin
 */

// include libs
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// include the reducers
import reducers from './reducers';

const store = createStore(reducers, applyMiddleware(thunk));

export default store;

