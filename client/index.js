/**
 * Main Client Application Entry point
 *
 * By: David Lin
 */

// include libs
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import store from './store.js';
import routes from './routes.js';
import styles from './styles/app.scss';

render(
    <Provider store={store}>
        {routes}
    </Provider>,
    document.getElementById('app')
);

