/**
 * Container for the Dedupe List
 * Connecting the List of deduped emails to the Redux Store
 *
 * By: David Lin
 */

// include libs
import React, { Component } from 'react';
import { connect } from 'react-redux';

// include actions to dispatch
import { generateEmails, dedupeEmails } from '../actions';

// include components to connect to
import Dedupe from '../components/dedupe.jsx';

// Map our reducer states to the component props
const mapStateToProps = (state) => {
    return {
        emails: state.emails,
        deduped: state.deduped
    };
};

// map our dispatch calls to component props
const mapDispatchToProps = (dispatch) => {
    return {
        generateEmails: (count) => {
            dispatch(generateEmails(count));
        },
        dedupeEmails: (emails) => {
            dispatch(dedupeEmails(emails));
        }
    };
};

// export the connection
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dedupe);

