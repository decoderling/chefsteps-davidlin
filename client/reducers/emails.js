/**
 * Redux Reducer for emails
 *
 * By: David Lin
 */

// emails reducer
const emails = (state = [], action) => {
    console.log('Reducer action', action.type);
    switch(action.type) {
        case 'RECEIVED_EMAILS':
            return action.emails;

        case 'API_ERROR':
            return [];

        default:
            return state;
    }
};

export default emails;

