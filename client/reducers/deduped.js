/**
 * Redux Reducer for deduped emails
 *
 * By: David Lin
 */

// deduped reducer
const deduped = (state = [], action) => {
    switch(action.type) {
        case 'RECEIVED_DEDUPE':
            return action.deduped;

        case 'API_ERROR':
            return [];

        default:
            return state;
    }
};

export default deduped;

