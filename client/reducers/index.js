/**
 * Redux Combined Reducer 
 *
 * By: David Lin
 */

// include libs
import { combineReducers } from 'redux';

// include reducers to combine
import emails from './emails.js';
import deduped from './deduped.js';

// with your reducers combined...
const reducers = combineReducers({
    emails,
    deduped
});

export default reducers;

