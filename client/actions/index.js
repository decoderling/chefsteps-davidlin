/**
 * Redux Actions to handle the Email Generation and Deduplication
 *
 * By: David Lin
 */

// include libs
import 'whatwg-fetch';

// -------[ Action Creators ]-------
export const receiveEmailSuccess = (emails) => {
    return {
        type: 'RECEIVED_EMAILS',
        emails
    };
};

export const receiveDedupeSuccess = (deduped) => {
    console.log('dedupe action', deduped);
    return {
        type: 'RECEIVED_DEDUPE',
        deduped
    };
};

export const receiveDataFailure = (error) => {
    return {
        type: 'API_ERROR',
        emails: [],
        error
    };
};

// -------[ Action Functions ]-------
// Makes an API call to generate emails based on the count
export function generateEmails(count) {
    return (dispatch) => {
        return fetch('/api/generate/' + count)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json.error) {
                    dispatch(receiveDataFailure(json));
                } else {
                    dispatch(receiveEmailSuccess(json));
                }
            })
            .catch((error) => {
                dispatch(receiveDataFailure(error));
            });
    };
};

// Makes an API call to dedupe the list of emails
export function dedupeEmails(emails) {
    return (dispatch) => {
        return fetch('/api/dedupe', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(emails)
            })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json.error) {
                    dispatch(receiveDataFailure(json));
                } else {
                    dispatch(receiveDedupeSuccess(json));
                }
            })
            .catch((error) => {
                dispatch(receiveDataFailure(error));
            });
    };
}
